# ECLIPSE CODE TEMPLATES

Templates de código para eclipse.

## 1 - Instalação
* 1.1 - Clone o repositório;
* 1.2 - No Eclipse acesse **"window > preferences"**;
* 1.3 - No menu lateral (esquerda) acesse **"Java > editor > templates"**;
* 1.4 - No menu lateral (direita) clique em **"importar"** (ou **"import"** caso o Eclipse esteja em inglês);
* 1.5 - Localize os arquivos de template clonados deste repositório e importe;
* 1.6 - Feche a janela de configurações;

## 2 - Uso
* 2.1 - No editor de código, pressionar **CTRL** + **Espaço** e digite uma das opções abaixo
	* 2.1.1 - CRUD (Gera o crud de uma classe);
	* 2.1.2 - List (Lista os objetos no banco);
	* 2.1.3 - Select (Seleciona um objeto pelo id);
	* 2.1.4 - Insert (Insere um objeto no banco);
	* 2.1.5 - Update (Atualizar um objeto do banco);
	* 2.1.6 - Delete (Remove um objeto do banco);
* 2.2 - Preencha as variáveis e pressione **TAB** para preencher a próxima